//window.alert("help");

var preg = ["Una condicional es cierta si su antecedente es falso y consecuente falso.", "Una equivalencia de P <--> Q es:", "Si P es Falsa", "P --> (Q --> not P) es falsa, entonces:", "Si Q = falsa y P = verdadera, entonces:", "Si P --> (Q --> R) es falsa, entonces:"]
var res1 = ["Falso", "P & Q", "P v Q es falsa", "P = falsa. Q = verdadera.", " P --> Q es verdadera.", "P = falsa. Q = falsa. R = falsa."]
var res2 = ["Verdadero"," not (P xor Q )", "P --> Q es verdadera", "P = Verdadera Q = verdadera", "not P --> Q es verdadera.", "P = verdadero. Q = verdadero. R = falsa."]
var res3 = ["Ambas", "P xot Q", "P --> Q es falsa" , "Q = falsa P = falsa", "  (not P) & (not Q) es verdadera.", " P = verdadero. Q = verdadero. R = verdadero."]
var res4 = ["No se", " ( not P) <--> (not Q)", "Q --> P es verdadera", "P = verdadera. Q = falsa", "not P v Q es verdadera.", "P = verdadero. Q = falsa. R = falsa."]

var sig = document.getElementById("siguiente")
var x = 0;
x++;

var c = ["Correcto", "Incorrecto"]

var s = 0;
s++;
var p = 0;
p++;
var cali;

function esconde(){
  document.getElementById("opcion1").style.display='none';
  document.getElementById("opcion2").style.display='none';
  document.getElementById("opcion3").style.display='none';
  document.getElementById("opcion4").style.display='none';
  document.getElementById("siguiente").style.display='none';
  document.getElementById("fint").style.display='none';
}

function empezar(){
  document.getElementById("opcion1").style.display='block';
  document.getElementById("opcion2").style.display='block';
  document.getElementById("opcion3").style.display='block';
  document.getElementById("opcion4").style.display='block';
  document.getElementById("siguiente").style.display='none';
  desaparecer1.innerHTML = "";
  mensaje1.innerHTML = "";
  pregu1.innerHTML = preg[x];
  opcion1.innerHTML = res1[x];
  opcion2.innerHTML = res2[x];
  opcion3.innerHTML = res3[x];
  opcion4.innerHTML = res4[x];
  numero1.innerHTML = p++;
  res.innerHTML = '';
}

function pregu1c(){
  res.innerHTML = c[0];
  punt.innerHTML = s++;
  document.getElementById("opcion1").style.display='none';
  document.getElementById("opcion2").style.display='none';
  document.getElementById("opcion3").style.display='none';
  document.getElementById("opcion4").style.display='none';
  document.getElementById("siguiente").style.display='block';
}

function siguiente(){
if (p == 6) {
  document.getElementById("pregu1").style.display='none';
  document.getElementById("siguiente").style.display='none';
  document.getElementById("fint").style.display='block';
  p = 5;
  document.getElementById("op").value = p;
  s = s-1;
  document.getElementById("or").value = s;
  cali = (s/p) *100;
  document.getElementById("oc").value = cali;
} else {
  siguiente.innerHTML = x++;
  document.getElementById("opcion1").style.display='block';
  document.getElementById("opcion2").style.display='block';
  document.getElementById("opcion3").style.display='block';
  document.getElementById("opcion4").style.display='block';
  document.getElementById("siguiente").style.display='none';
  pregu1.innerHTML = preg[x];
  opcion1.innerHTML = res1[x];
  opcion2.innerHTML = res2[x];
  opcion3.innerHTML = res3[x];
  opcion4.innerHTML = res4[x];
  numero1.innerHTML = p++;
}
}

if (sig == "siguiente"){
 x++;
}

function pregu2c(){
  res.innerHTML = c[1];
  punt.innerHTML = s-1;
  document.getElementById("opcion1").style.display='none';
  document.getElementById("opcion2").style.display='none';
  document.getElementById("opcion3").style.display='none';
  document.getElementById("opcion4").style.display='none';
  document.getElementById("siguiente").style.display='block';
}

function pregu3c(){
  res.innerHTML = c[1];
  punt.innerHTML = s-1;
  document.getElementById("opcion1").style.display='none';
  document.getElementById("opcion2").style.display='none';
  document.getElementById("opcion3").style.display='none';
  document.getElementById("opcion4").style.display='none';
  document.getElementById("siguiente").style.display='block';
}

function pregu4c(){
  res.innerHTML = c[1];
  punt.innerHTML = s-1;
  document.getElementById("opcion1").style.display='none';
  document.getElementById("opcion2").style.display='none';
  document.getElementById("opcion3").style.display='none';
  document.getElementById("opcion4").style.display='none';
  document.getElementById("siguiente").style.display='block';
}
